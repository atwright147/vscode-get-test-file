# Get Test File

Do you like to debug your tests? If so, you have probably felt the frustration of accidentally hitting `F5` whilst the file under test still has focus and then your test runner says it can't find any tests.

Well, **Get Test File** might just be the solution. Simply name your **files** and **test files** the same, but with "spec" before the file extension and this vscode extension will figure out what to run for you.

## Usage

Use the command `${command:getTestFile}` in your `.vscode/launch.json` file or via `ctrl/cmd+shift+p`.

### launch.json example

```json
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "launch",
            "name": "Unit Tests: Current File",
            "program": "${workspaceRoot}/node_modules/.bin/_mocha",
            "cwd": "${workspaceRoot}",
            "args": [
                "-u", "tdd",
                "--timeout=999999",
                "--colors",
                "--opts", "${workspaceRoot}/mocha.opts",
                "${command:getTestFile}"
            ],
        }
    ]
}
```

## Features

- If your **files** and **test files** follow the following pattern, then you will be able to just press `F5` and automatically run your test file whether `file.js` or `file.spec.js` currently has focus:

    ```js
    folder
        file.js
        file.spec.js
    ```

- Designed to be file type agnostic, so it should work with `.js`, `.ts`, `.py` or anything else. However, as I only really work with JavaScript I have not tested this.

### Planed features

- [ ] Make the test file filename marker configurable, it is currently hardcoded to work with `.spec.js` but `.test.js` is popular too.
- [ ] Make the path to test files configurable, it is currently hardcoded to assume the test file is on the same path as the focussed file.

<!--
## Requirements

If you have any requirements or dependencies, add a section describing those and how to install and configure them.

## Extension Settings

Include if your extension adds any VS Code settings through the `contributes.configuration` extension point.

For example:

This extension contributes the following settings:

* `myExtension.enable`: enable/disable this extension
* `myExtension.thing`: set to `blah` to do something

## Known Issues

Calling out known issues can help limit users opening duplicate issues against your extension.
-->

## Release Notes

### 0.1.0

Initial release
