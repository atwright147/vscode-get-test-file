/* global suite, test */

const { expect } = require('chai');
const sinon = require('sinon');
const { getTestFileString } = require('../getTestFileString');

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
// const vscode = require('vscode');
// const myExtension = require('../extension');

suite('#getTestFileString', function() {
    const testFileMarker = 'spec';

    test('should return a test file when a non-test file has focus', function() {
        expect(getTestFileString('/path/to/file.js', testFileMarker)).to.equal(`/path/to/file.${testFileMarker}.js`);
    });

    test('should return the same test file when a test file has focus', function() {
        expect(getTestFileString(`/path/to/file.${testFileMarker}.js`, testFileMarker)).to.equal(`/path/to/file.${testFileMarker}.js`);
    });
});
