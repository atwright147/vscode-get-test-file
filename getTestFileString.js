const path = require('path');

const getTestFileString = function(filename, testFileMarker) {
    const fileInfo = path.parse(filename);
    const splitFileName = fileInfo.name.split('.');
    const isSpecFile = splitFileName[splitFileName.length - 1] === testFileMarker;

    if (isSpecFile) {
        return filename;
    } else {
        const specFilePath = `${fileInfo.dir}/${fileInfo.name}.${testFileMarker}${fileInfo.ext}`;
        return specFilePath;
    }
}

exports.getTestFileString = getTestFileString;
